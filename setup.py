from setuptools import setup

packages = [
    'cce_permissions',
    'cce_formtools',
    'cce_extraviews',
    'cce_utils',
    "content_repository",
]

setup(
    name='cce_extras',
    version='0.2',
    description='A collection of tools for use in cce projects',
    url='http://subversion.outreach.cce/svn/it/django/cce_extras',
    author='CCE Devs',
    author_email='wsmith@cs.ou.edu',
    license='All Rights Reserved',
    packages=packages,
    zip_safe=False,
)
