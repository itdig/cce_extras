from itertools import chain
from django import forms
from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe
from django.utils.html import escape, conditional_escape

class CheckboxSelectMultipleHorizontal(forms.CheckboxSelectMultiple):
    """
    A widget that renders its checkboxes horizontally instead of
    vertically.

    Set the NUM_COLS class var to control the number of columns
    that will be used; default is -1, which will place all
    items on the same row.
    """
    NUM_COLS = 3

    def __init__(self, *args, **kwargs):
        self.NUM_COLS = kwargs.pop('num_cols', self.NUM_COLS)
        super(CheckboxSelectMultipleHorizontal, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None, choices=()):
        if value is None: value = []
        has_id = attrs and 'id' in attrs
        final_attrs = self.build_attrs(attrs, name=name)
        output = [u"<ul class='horizontal_checkbox'>"]
        # Normalize to strings
        str_values = set([force_unicode(v) for v in value])
        for i, (option_value, option_label) in enumerate(chain(self.choices, choices)):
            # If an ID attribute was given, add a numeric index as a suffix,
            # so that the checkboxes don't all have the same ID attribute.
            if has_id:
                final_attrs = dict(final_attrs, id='%s_%s' % (attrs['id'], i))
                label_for = u' for="%s"' % final_attrs['id']
            else:
                label_for = ''

            cb = forms.CheckboxInput(final_attrs, check_test=lambda value: value in str_values)
            option_value = force_unicode(option_value)
            rendered_cb = cb.render(name, option_value)
            option_label = conditional_escape(force_unicode(option_label))
            output.append(u"<li class='horizontal_checkbox' style='display: inline-block;'><label%s>%s %s</label></li>" % (label_for, rendered_cb, option_label))
            if self.NUM_COLS > 0 and (i + 1) / self.NUM_COLS >= 1 and (i + 1) % self.NUM_COLS == 0 :
                output.append("<br />")
        output.append(u'</ul>')
        return mark_safe(u'\n'.join(output))