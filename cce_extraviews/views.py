from django.views.generic import CreateView
from django.http.response import HttpResponseRedirect


class MultipleModelCreateView(CreateView):
    """
    This View is for dynamically adding modelforms for several
    related, but different, models other than the one
    defined on a regular CreateView.

    You need to define a list of functions in the instance
    var related_forms, and then implement those functions.

    The mixin will then add them to the context, validate them in
    a submission, etc.
    """
    get_related_forms = []
    object_field_name = None

    def _get_related_forms(self, request):
        forms = []
        for form in self.get_related_forms:
            if hasattr(self, form):
                func = getattr(self, form)
                if callable(func):
                    forms.append(func(request))
        return forms

    def _save_related_forms(self, forms):
        for form in forms:
            obj = form.save(commit=False)
            if hasattr(obj, self.object_field_name):
                setattr(obj, self.object.field_name, self.object)
                obj.save()
        return


    def post(self, request, *args, **kwargs):
        self.object = None
        forms = self._get_related_forms(self.request)
        forms.append(self.get_form(self.get_form_class()))
        all_valid = []
        for form in forms:
            if not form.is_valid():
                all_valid.append(False)
            else:
                all_valid.append(True)
        if all(all_valid):
            return self.form_valid(forms[-1])
        return self.form_invalid(forms[-1])

    def form_valid(self, form):
        forms = self._get_related_forms(self.request)
        self.object = form.save()
        self._save_related_forms(forms)
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(MultipleModelCreateView, self).get_context_data(**kwargs)
        forms = self._get_related_forms(self.request)
        context.update({
            'related_forms': forms
        })
        return context