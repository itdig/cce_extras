from django.core.exceptions import ImproperlyConfigured, PermissionDenied


class PermissionsRequiredMixin(object):
    """
    Inspired by django-braces: https://github.com/brack3t/django-braces

    Requires django-rulez https://github.com/chrisglass/django-rulez

    Parent class of view mixins which allow you to specify a list of rules for
    access to a resource (model or instance of a model, currently)
    via django-rulez by http verb.  Each permission
    that is listed will be required (logical AND for multiple cce_permissions).

    A 403 will be raised if any of the checks fails.

    Simply create a dictionary with http verbs as keys, and
    each of their values should be a list of functions (as strings)
    in 'function_name' format.  Each permission should be a function
    in the same class as the model or model instance defined on
    the view that inherits one of the children mixins.

    Example Usage on an UpdateView subclass:

        cce_permissions = {
            "get": ["add_object"]
            "post": ["change_object", "delete_object"]
        }


    """
    permissions = None  # Default required perms to none

    def dispatch(self, request, *args, **kwargs):
        # some things we use, like get_object(), need the following
        self.kwargs = kwargs

        # sanity checks
        self._check_permissions_attr()
        self._check_perms_keys()
        self._check_resource()

        # check perms here
        if request.method.lower() in self.permissions.keys():
            for func in self.permissions.get(request.method.lower()):
                self._check_permissions(request, func)

        return super(PermissionsRequiredMixin, self).dispatch(
            request, *args, **kwargs)

    def _check_permissions(self, request, permission):
        """
        This method is responsible for calling the permission
        checking functions.
        """
        raise NotImplementedError(
            "You must define and implement a '_check_permissions' method"
        )

    def _check_resource(self):
        """
        This method is responsible for calling the implementing mixin's
        method that verifies the resource is defined; aka get_object()
        or model exists.
        """
        raise NotImplementedError(
            "You must define and implement a '_check_resource' method"
        )

    def _check_permissions_attr(self):
        """
        This method checks that the cce_permissions attribute
        is set and that it is a dict.
        """
        if self.permissions is None or not isinstance(self.permissions, dict):
            raise ImproperlyConfigured(
                "'PermissionsRequiredMixin' requires "
                "'cce_permissions' attribute to be set to a dict.")

    def _check_perms_keys(self):
        """
        This method checks that the listed permission checking functions
        on the resources exist.
        """
        raise NotImplementedError(
            "You must implement a _check_perms_keys method"
        )


class ObjectPermissionsMixin(PermissionsRequiredMixin):
    """
    Use this mixin when get_object() is called in your view;
    i.e. DetailView, UpdateView, etc.

    This class should implement three methods:

    _check_resource()
        should verify that the resource we wish to protect
        (object, model, etc.) exists

    _check_perms_keys()
        should verify that the permission functions exist

    _check_permissions()
        should actually make the call to the defined cce_permissions
    """

    def _check_permissions(self, request, permission):
        resource = self.get_object()
        permission_func = getattr(resource, permission)
        if not permission_func(request.user):
            raise PermissionDenied

    def _check_resource(self):
        self._check_get_object()

    def _check_perms_keys(self):
        """
        If the cce_permissions list/tuple passed in is set, check to make
        sure that it is of the type list or tuple.
        """
        for k, v in self.permissions.iteritems():
            if not v:
                raise ImproperlyConfigured(
                    "'PermissionsRequiredMixin' requires"
                    "'cce_permissions' functions list to have at least one item.")
            for func in v:
                if getattr(self.get_object(), func, None) is None:
                    raise ImproperlyConfigured(
                    "Cannot locate %s.%s; does it exist?" % (self.get_object(), func))

    def _check_get_object(self):
        """
        Make sure get_object exists
        """
        if getattr(self, 'get_object', None) is None:
            raise ImproperlyConfigured(
                "'ObjectPermissionsMixin' requires a get_object method"
            )


class ClassPermissionsMixin(PermissionsRequiredMixin):
    """
    Use this mixin when table-level cce_permissions are required;
    i.e. CreateView, etc.

    This class should implement three methods:

    _check_resource()
        should verify that the resource we wish to protect
        (object, model, etc.) exists

    _check_perms_keys()
        should verify that the permission functions exist

    _check_permissions()
        should actually make the call to the defined cce_permissions
    """

    def _check_permissions(self, request, permission):
        resource = self.model()
        permission_func = getattr(resource, permission)
        if not permission_func(request.user):
            raise PermissionDenied

    def _check_resource(self):
        self._check_model()

    def _check_perms_keys(self):
        """
        If the cce_permissions list/tuple passed in is set, check to make
        sure that it is of the type list or tuple.
        """
        for k, v in self.permissions.iteritems():
            if not v:
                raise ImproperlyConfigured(
                    "'PermissionsRequiredMixin' requires"
                    "'cce_permissions' functions list to have at least one item.")
            for func in v:
                if getattr(self.model, func, None) is None:
                    raise ImproperlyConfigured(
                    "Cannot locate %s.%s; does it exist?" % (self.model, func))

    def _check_model(self):
        """
        Make sure model is defined on CBV
        """
        if getattr(self, 'model', None) is None:
            raise ImproperlyConfigured(
                "'ObjectPermissionsMixin' requires a model attribute"
                "to be defined on the class"
            )
