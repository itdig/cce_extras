from django.db import models


class Ingredient(models.Model):

    CATEGORY_OPTIONS = (
        (0, 'Vegetable'),
        (1, 'Meat'),
        (2, 'Cheese'),
    )

    name = models.CharField(unique=True, max_length=50)
    quantity = models.IntegerField(default=0)
    category = models.CharField(max_length=1, default=0, choices=CATEGORY_OPTIONS)
    calories_per_topping = models.IntegerField(default=0)

    @classmethod
    def get_quantity(cls, name):
        try:
            quantity = cls.objects.get(name=name).quantity
        except cls.DoesNotExist:
            quantity = 0
        return quantity

    @classmethod
    def get_category_count(cls, category):
        return cls.objects.filter(category=category).count()

    @classmethod
    def total_toppings_calories(cls, toppings_list):
        total = 0
        if isinstance(toppings_list, list):
            for topping in toppings_list:
                if isinstance(topping, cls):
                    total += topping.calories_per_topping
        return total

    def decrease_quantity(self, quantity_decrease_value):
        try:
            quantity_decrease_value = int(quantity_decrease_value)
        except ValueError:
            pass
        else:
            self.quantity -= quantity_decrease_value
            self.save()

    def increase_quantity(self, quantity_decrease_value):
        try:
            quantity_decrease_value = int(quantity_decrease_value)
        except ValueError:
            pass
        else:
            self.quantity += quantity_decrease_value
            self.save()

class Pizza(models.Model):

    name = models.CharField(max_length=50)
    topping_set = models.ManyToManyField(Ingredient)


class Calzone(models.Model):

    name = models.CharField(max_length=50)
    topping_set = models.ManyToManyField(Ingredient)


class Reporter(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return "%s" % self.name


class Article(models.Model):
    pub_date = models.DateField()
    headline = models.CharField(max_length=200)
    content = models.TextField()
    reporter = models.ForeignKey(Reporter)

    def __unicode__(self):
        return "%s by %s" % (self.headline, self.reporter)