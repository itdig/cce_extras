from django.conf.urls import patterns, url

from .views import (
    AssociatedObjectsListView,
    #MultipleModelListView,
    ViewForTestingTests,
    article_searchform_view,
    article_modelform_view,
    reporter_modelform_view,
    article_modelformset_view,
    article_inlineformset_view
)

urlpatterns = patterns(
    '',
    url(r'^$', AssociatedObjectsListView.as_view(), name='multiple_model_list'),
    url(r'^test/$', ViewForTestingTests.as_view(), name='test_view'),
    url(r'^article_form/$', article_modelform_view, name='article_form_view'),
    url(r'^article_formset/$', article_modelformset_view, name='article_formset_view'),
    url(r'^article_inlineformset/$', article_inlineformset_view, name='article_inlineformset_view'),
    url(r'^reporter_form/$', reporter_modelform_view, name='reporter_form_view'),
    url(r'^article_search_form/$', article_searchform_view, name='article_search_form_view'),
)
