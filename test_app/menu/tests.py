from django.test import TestCase
from menu.models import Ingredient


class IngredientModelTest(TestCase):
    def testing_fetching_ingredients_quantity(self):
        qty = 5
        Ingredient(name="tomato", quantity=qty).save()
        qty2 = Ingredient.get_quantity("tomato")
        self.assertEquals(qty, qty2)

    def testing_fetching_bad_ingredient_quantity_returns_null(self):
        qty = Ingredient.get_quantity("xx")
        self.assertEquals(0, qty)

    def testing_fetching_ingredients_count_per_category(self):
        Ingredient(name="Pepper Jack cheese", category=2, quantity=1).save()
        Ingredient(name="American cheese", category=2, quantity=2).save()
        cheese_quantity = Ingredient.get_category_count(2)
        self.assertEquals(cheese_quantity, 2)

    def testing_decreasing_ingredient_quantity(self):
        name = "American cheese"
        american_cheese_initial_qty = 10
        american_cheese_decrease_value = 5
        american_cheese = Ingredient(name=name, category=2, quantity=american_cheese_initial_qty)
        american_cheese.save()
        american_cheese.decrease_quantity(american_cheese_decrease_value)
        american_cheese_qty = Ingredient.get_quantity(name)
        self.assertEquals(american_cheese_qty, (american_cheese_initial_qty - american_cheese_decrease_value))

    def testing_decreasing_ingredient_quantity_by_passing_bad_quantity(self):
        name = "American cheese"
        american_cheese_initial_qty = 10
        american_cheese_decrease_value = "a"
        american_cheese = Ingredient(name=name, category=2, quantity=american_cheese_initial_qty)
        american_cheese.save()
        american_cheese.decrease_quantity(american_cheese_decrease_value)
        american_cheese_qty = Ingredient.get_quantity(name)
        self.assertEquals(american_cheese_qty, american_cheese_initial_qty)

    def testing_increasing_ingredient_quantity_by_passing_bad_quantity(self):
        name = "American cheese"
        american_cheese_initial_qty = 10
        american_cheese_increase_value = "a"
        american_cheese = Ingredient(name=name, category=2, quantity=american_cheese_initial_qty)
        american_cheese.save()
        american_cheese.increase_quantity(american_cheese_increase_value)
        american_cheese_qty = Ingredient.get_quantity(name)
        self.assertEquals(american_cheese_qty, american_cheese_initial_qty)

    def testing_fetching_total_calories_for_list_of_ingredients(self):
        pepper_jack_cheese_calories = 200
        pepper_jack_cheese = Ingredient(name="Pepper Jack cheese",
                                        category=2,
                                        calories_per_topping=pepper_jack_cheese_calories)
        pepper_jack_cheese.save()
        american_cheese_calories = 200
        american_cheese = Ingredient(name="American cheese",
                                     category=2,
                                     calories_per_topping=american_cheese_calories)
        american_cheese.save()
        toppings = [pepper_jack_cheese, american_cheese]
        total_toppings_calories = Ingredient.total_toppings_calories(toppings)

        self.assertEquals(total_toppings_calories, (american_cheese_calories + pepper_jack_cheese_calories))

    def testing_fetching_total_calories_for_list_of_ingredients_with_non_list_parameter(self):
        toppings = "a"
        total_toppings_calories = Ingredient.total_toppings_calories(toppings)
        self.assertEquals(total_toppings_calories, 0)

    def testing_fetching_total_calories_for_list_of_ingredients_with_bad_list(self):
        pepper_jack_cheese_calories = 200
        pepper_jack_cheese = Ingredient(name="Pepper Jack cheese",
                                        category=2,
                                        calories_per_topping=pepper_jack_cheese_calories)
        pepper_jack_cheese.save()
        toppings = ["a", 0, pepper_jack_cheese]
        total_toppings_calories = Ingredient.total_toppings_calories(toppings)
        self.assertEquals(total_toppings_calories, pepper_jack_cheese_calories)
