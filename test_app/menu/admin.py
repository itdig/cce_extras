from django.contrib import admin
from .models import (
    Pizza,
    Calzone,
    Ingredient,
    Article,
    Reporter
)

admin.site.register(Ingredient)
admin.site.register(Pizza)
admin.site.register(Calzone)
admin.site.register(Article)
admin.site.register(Reporter)
