from django.views.generic import ListView
from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.forms.models import modelformset_factory, inlineformset_factory
from .models import Pizza, Article, Reporter
from django.views.generic.edit import FormView
from .forms import FormForTestingTests, ArticleForm, ReporterModelForm, ArticleModelForm


class AssociatedObjectsMixin(object):

    def get_related_querysets(self):
        return []


class AssociatedObjectsListView(AssociatedObjectsMixin, ListView):
    """
    Get a list of objects from the "main" object

    Override the get_related_querysets() method to return
    a list of related querysets that you want in the template
    """
    model = Pizza
    template_name = "multiple_object_list.html"

    def get_context_data(self, **kwargs):
        context = super(AssociatedObjectsListView, self).get_context_data(**kwargs)
        context.update({
            'associated_objects': self.get_related_querysets()
        })
        return context

class ViewForTestingTests(FormView):
    template_name = "test_form_test.html"
    form_class = FormForTestingTests

def form_test_view(request, *args, **kwargs):
    form = None
    if request.method == 'GET':
        form = ArticleForm(request.GET or None)
    else:
        form = ArticleForm(request.POST)
    return render_to_response("form_example.html",
                              {'form': form},
                              context_instance=RequestContext(request))


def article_searchform_view(request, *args, **kwargs):
    form, qs = None, None
    if request.method == 'GET':
        form = ArticleForm(request.GET or None)
        if form.is_valid():
            qs = Article.objects.filter(headline__icontains=form.cleaned_data.get('headline'))
    return render_to_response("form_example.html",
                              {'form': form, 'method': 'get', 'objects': qs, 'show_results': True},
                              context_instance=RequestContext(request))


def article_modelform_view(request, *args, **kwargs):
    form = None
    if request.method == 'GET':
        form = ArticleModelForm(request.GET or None)
    else:
        form = ArticleModelForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('article_form_view'))
    return render_to_response("form_example.html",
                              {'form': form, 'method': 'post'},
                              context_instance=RequestContext(request))


def article_modelformset_view(request, *args, **kwargs):
    if request.method == 'GET':
        ArticleModelFormSet = modelformset_factory(Article)
        #formset = ArticleModelFormSet(request.GET or None,
        #                              queryset=Article.objects.filter(headline__startswith='O'))
        formset = ArticleModelFormSet(request.GET or None)
    else:
        ArticleModelFormSet = modelformset_factory(Article)
        #formset = ArticleModelFormSet(request.POST,
        #                              queryset=Article.objects.filter(headline__startswith='O'))
        formset = ArticleModelFormSet(request.POST)
        if formset.is_valid():
            formset.save()
            return redirect(reverse('article_formset_view'))
    return render_to_response("formset_example.html",
                              {'formset': formset, 'method': 'post'},
                              context_instance=RequestContext(request))


def article_inlineformset_view(request, *args, **kwargs):
    if request.method == 'GET':
        ArticleInlineFormSet = inlineformset_factory(Reporter, Article, extra=0)
        reporter = Reporter.objects.get(name='Wayne Smith')
        formset = ArticleInlineFormSet(request.GET or None,
                                      instance=reporter)
    else:
        ArticleInlineFormSet = inlineformset_factory(Reporter, Article, extra=0)
        reporter = Reporter.objects.get(name='Wayne Smith')
        formset = ArticleInlineFormSet(request.POST,
                                      instance=reporter)
        if formset.is_valid():
            formset.save()
            return redirect(reverse('article_inlineformset_view'))
    return render_to_response("formset_example.html",
                              {'formset': formset, 'method': 'post'},
                              context_instance=RequestContext(request))


def reporter_modelform_view(request, *args, **kwargs):
    form = None
    if request.method == 'GET':
        form = ReporterModelForm(request.GET or None)
    else:
        form = ReporterModelForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('reporter_form_view'))
    return render_to_response("form_example.html",
                              {'form': form, 'method': 'post'},
                              context_instance=RequestContext(request))
