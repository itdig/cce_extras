from django import forms
from .models import Article, Reporter


class FormForTestingTests(forms.Form):
    name = forms.CharField()
    message = forms.CharField(widget=forms.Textarea)


class ArticleForm(forms.Form):
    headline = forms.CharField(label='Headline to search', max_length=100)


class ArticleModelForm(forms.ModelForm):
    class Meta:
        model = Article


class ReporterModelForm(forms.ModelForm):
    class Meta:
        model = Reporter