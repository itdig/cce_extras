from django.test import TestCase
import os
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time


class SetupTeardownMixin(object):
    def setUp(self):
        self.browser = webdriver.Firefox()
        return super(SetupTeardownMixin, self).setUp()

    def tearDown(self):
        self.browser.quit()
        return super(SetupTeardownMixin, self).tearDown()

class IngredientsList(SetupTeardownMixin, LiveServerTestCase):
    # TODO: list template
    # Adonis
    def test_empty_list(self):
        # TODO: test when the database has no ingredients
        self.browser.get(self.live_server_url + "/")
        time.sleep(1)

    # TODO: ingredients templates
    def test_one_ingredient(self):
        self.browser.get(self.live_server_url + "/")
        time.sleep(2)

    def test_multiple_ingredients(self):
        pass

    def test_anonymous_users_list_ingredients(self):
        # should this succeed or fail?
        pass

class IngredientsCreate(SetupTeardownMixin, LiveServerTestCase):
    # TODO: form template
    # TODO: ingredient form
    # montana
    def test_admin_can_create_ingredients(self):
        pass
    def test_anonymous_cannot_create_ingredients(self):
        pass

class IngredientsUpdate(SetupTeardownMixin, LiveServerTestCase):
    # TODO: validation
    # montana
    def test_admin_can_update_existing_ingredient(self):
        pass
    def test_fail_to_update_nonexistent_ingredient(self):
        pass
    def test_anonymous_cannot_update_ingredients(self):
        pass
    def test_fail_to_invalidate_ingredient(self):
        pass

