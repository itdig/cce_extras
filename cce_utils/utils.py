import os
import datetime
import mimetypes
from django.http import Http404, HttpResponse, HttpResponseForbidden
from django.conf import settings


def return_file_response(backing_file):
        """
        For a given file object (backing_file), return
        an Http response for download
        """
        if not backing_file:
            raise Http404("The resource requested is no longer available")
        mimetype = mimetypes.guess_type(backing_file.url)
        # better would be to capture mimetype at upload time
        response = HttpResponse(mimetype=mimetype)
        response.write(backing_file.read())
        response['Content-Disposition'] = 'attachment; filename=%s' % os.path.basename(backing_file.name)
        return response

def serve_decorate(model, field, permission_function='can_download_resource'):
    """
    Return a file stored as attribute 'field' on 'model'

    Optionally call a permission function (passed as a string) if given
    """
    def result(request, path, document_root, show_indexes=False):
        # set the path on server where things are stored
        # this should be relative to MEDIA_ROOT
        leading_path = getattr(settings, 'SERVE_DECORATE_PROTECTED_DIR', "restricted/file_resources")
        # strip a possible trailing slash
        if path[-1] == '/':
            path = path[:-1]
        file_resources = []
        for fr in model.objects.all():
            file_obj = getattr(fr, field, None)
            if file_obj is not None:
                if file_obj.name == ("%s/%s" % (leading_path, path)):
                    file_resources.append(fr)
        if not file_resources: raise Http404
        resource = file_resources[0]
        can_download = True
        perm_func = getattr(resource, permission_function, None)
        if perm_func is not None and callable(perm_func):
            can_download = resource.can_download_resource(request.user)
        if not can_download:
            return HttpResponseForbidden("You do not have permission to download this resource")
        return return_file_response(resource.file)
    return result


def create_ranges(list_of_dates):
        """
        -Creates ranges from sparse list containing datetime objects that are sorted in ascending order
        -Input:
            Accepts a sorted list of dates i.e datetime objects
        -Output:
            Gives back a list of ranges
	- Requires post processing on the resultant date ranges
	- A range is nothing but a bunch of dates clumped together
        in  a list if they occur within or after 24 hours.
        - So the resultant list may look like this 
        [[2014-5-1, 2014-5-2, 2014-5-3, 2014-5-4], [2014-5-8, 2014-5-9], [2014-5-11], [2014-5-14, 2014-5-15]]
        """

        ranges = []
        sub_range = []
        last_element = False
        for i in range(0, len(list_of_dates)+1):
            try:
                every_date = list_of_dates[i]
            except IndexError:
                every_date = list_of_dates[-1]
                last_element = True
            #print every_date, sub_range
            if sub_range:
                if every_date - sub_range[-1] > datetime.timedelta(1):
                    ranges.append(sub_range)
                    sub_range = []
                    sub_range.append(every_date)
                else:
                    if not last_element:
                        sub_range.append(every_date)
                    if last_element:
                        ranges.append(sub_range)
            else:
                sub_range.append(every_date)
        return ranges
