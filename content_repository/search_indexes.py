from haystack import indexes
from .models import FileResource


class FileResourceIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True)
    filename = indexes.EdgeNgramField(model_attr='title')
    description = indexes.EdgeNgramField(model_attr='description')
    author = indexes.EdgeNgramField(model_attr='author')
    date_uploaded = indexes.DateTimeField(model_attr='date_uploaded')
    category = indexes.EdgeNgramField(model_attr='category')
    tags = indexes.EdgeNgramField(model_attr='tags')

    def get_model(self):
        return FileResource