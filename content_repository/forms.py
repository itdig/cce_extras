from django import forms
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from .models import FileResource, KnowledgeBaseCategory


class FileResourceForm(forms.ModelForm):

    class Meta:
        model = FileResource
        exclude = ('author', 'tags')
        widgets = {
            'tags': forms.CheckboxSelectMultiple
        }

    tag_1 = forms.CharField(max_length=100, required=False)
    tag_2 = forms.CharField(max_length=100, required=False)
    tag_3 = forms.CharField(max_length=100, required=False)

    def __init__(self, *args, **kwargs):
    	self.request = kwargs.pop('request', None)
    	self.file_resource = kwargs.pop('file_resource', None)
    	super(FileResourceForm, self).__init__(*args, **kwargs)
    	if self.request is not None and self.file_resource is not None:
    		if self.request.user == self.file_resource.author:
    			ctr = 1
    			tags = self.file_resource.tags.all()
    			while ctr < 4:
    				tag_name = ''
    				try:
    					tag_name = tags[ctr - 1]
    				except IndexError:
    					pass
    				self.fields['tag_%s' % ctr] = forms.CharField(max_length=100, required=False, initial=tag_name)
    				ctr += 1
    	self.fields['category'].queryset = KnowledgeBaseCategory.objects.all().order_by('name').filter(active=True)


    def clean_title(self):
        title = self.cleaned_data['title']
        try:
            fr = FileResource.objects.get(title=title)
            if fr.pk != self.instance.pk:
                msg = u"Title field must be unique; please try another title"
                self._errors["title"] = self.error_class([msg])
        except ObjectDoesNotExist:
            pass # this is ok
        except MultipleObjectsReturned:
            # this is not
            msg = u"Title field must be unique; please try another title"
            self._errors["title"] = self.error_class([msg])
        return title

    def clean(self):
    	cleaned_data = self.cleaned_data
    	if not cleaned_data.get('tag_1') and not cleaned_data.get('tag_2') and not cleaned_data.get('tag_3'):
    		raise forms.ValidationError("You must enter atleast one tag")
    	return cleaned_data


class FileResourceFilterForm(forms.Form):
	"""
	This form has all the filter options
	"""

	category = forms.ModelChoiceField(queryset=KnowledgeBaseCategory.objects.all().order_by('name').filter(active=True), empty_label=None)

	def __init__(self, *args, **kwargs):
		super(FileResourceFilterForm, self).__init__(*args, **kwargs)