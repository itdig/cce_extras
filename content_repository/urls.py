from django.conf.urls.defaults import patterns, url
from django.contrib.auth.decorators import login_required
from .views import KnowledgeBaseTemplateView, FileResourceListView, FileResourceCreateView, \
    FileResourceDetailView, FileResourceUpdateView, FileResourceDownloadView, \
    FileResourcePreviousVersionListView, FileResourcePreviousVersionDetailView, \
    FileResourcePreviousVersionDownloadView, FileResourceDeleteView, FileResourcePreviousVersionDeleteView

urlpatterns = patterns('',
                       url(r'^$', login_required(KnowledgeBaseTemplateView.as_view()), name='knowledge_base'),
                       url(r'^resources/$', login_required(FileResourceListView.as_view()), name='file_resource_list'),
                       url(r'^resources/create/$', login_required(FileResourceCreateView.as_view()), name='file_resource_create'),
                       url(r'^resources/update/(?P<pk>\d+)/$', login_required(FileResourceUpdateView.as_view()), name='file_resource_update'),
                       url(r'^resources/download/(?P<pk>\d+)/$', login_required(FileResourceDownloadView.as_view()), name='file_resource_download'),
                       url(r'^resources/(?P<pk>\d+)/$', login_required(FileResourceDetailView.as_view()), name='file_resource_detail'),
                       url(r'^resources/(?P<pk>\d+)/delete/$', login_required(FileResourceDeleteView.as_view()), name='file_resource_delete'),

                       # previous versions
                       url(r'^resources/(?P<current_version_pk>\d+)/previous/$', login_required(FileResourcePreviousVersionListView.as_view()), name='file_resource_previous_version_list'),
                       url(r'^resources/(?P<current_version_pk>\d+)/previous/(?P<pk>\d+)/$', login_required(FileResourcePreviousVersionDetailView.as_view()), name='file_resource_previous_version_detail'),
                       url(r'^resources/(?P<current_version_pk>\d+)/previous/(?P<pk>\d+)/download/$', login_required(FileResourcePreviousVersionDownloadView.as_view()), name='file_resource_previous_version_download'),
                       url(r'^resources/(?P<current_version_pk>\d+)/previous/(?P<pk>\d+)/delete/$', login_required(FileResourcePreviousVersionDeleteView.as_view()), name='file_resource_previous_version_delete'),
                       )