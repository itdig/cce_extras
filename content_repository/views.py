from django.views.generic import TemplateView, ListView, CreateView, DetailView, UpdateView, DeleteView
from django.http import HttpResponseRedirect, HttpResponseForbidden, Http404
from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.core.exceptions import FieldError
from cce_utils.utils import return_file_response
from cce_permissions.views import ObjectPermissionsMixin, ClassPermissionsMixin
from .models import FileResource, KnowledgeBaseTag, FileResourcePreviousVersion
from .forms import FileResourceForm, FileResourceFilterForm


class KnowledgeBaseTemplateView(TemplateView):
    template_name = "2014/repository/templates/knowledge_base.html"


class FileResourceListView(ListView):
    template_name = "2014/repository/templates/fileresource_list.html"
    model = FileResource
    paginate_by = 10

    def get_queryset(self):
        qs = super(FileResourceListView, self).get_queryset()
        query_string = self.request.META['QUERY_STRING']
        query_dict = {}
        if not query_string == "":
            if not query_string.__contains__("page"):
                if query_string.__contains__('&'):
                    split_amp = query_string.split("&")
                    #if the string has & it will give pieces in a list
                    #if not the string is passed as it is and only one element in the lsit
                    if not len(split_amp) == 1:
                        for i in split_amp:
                            x = i.split("=")
                            query_dict[x[0]] = x[1]
                    else:
                        query_dict[split_amp.split("=")[0]] = split_amp.split('=')[1]
                else:
                    query_dict[query_string.split('=')[0]] = query_string.split('=')[1]
                try:
                    qs = qs.filter(**query_dict)
                except FieldError:
                    raise Http404

        #the query_dict has the keyword args and values accordingly
        # extra_filter = self.request.GET.get('filter', None)
        # if extra_filter is not None:
        #     value = self.request.GET.get('value', None)
        #     if value is not None:
        #         try:
        #             qs = qs.filter(**{extra_filter: value})
        #             self.active_tab = extra_filter
        #         except FieldError:
        #             raise Http404
        return qs

    def get(self, request, *args, **kwargs):
        if not request.user.has_perm('can_view_list', FileResource()):
            return HttpResponseForbidden("You cannot view the repository")
        return super(FileResourceListView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(FileResourceListView, self).get_context_data(**kwargs)
        context.update({
            'resource_list_view': True,
            'category_form' : FileResourceFilterForm(self.request.GET or None),
        })
        return context


class FileResourceCreateView(ClassPermissionsMixin, CreateView):
    template_name = "2014/repository/templates/file_resource.html"
    model = FileResource
    form_class = FileResourceForm
    permissions = {
        'get': ['can_edit'],
        'post': ['can_edit']
    }

    def get_success_url(self):
        return reverse('file_resource_list')

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.author = self.request.user
        obj.save()
        self.object = obj
        if form.cleaned_data.get('tag_1') and form.cleaned_data.get('tag_1').replace(' ', ''):
            self.object.tags.add(KnowledgeBaseTag.get_or_create_tag(form.cleaned_data.get('tag_1')))
        if form.cleaned_data.get('tag_2') and form.cleaned_data.get('tag_2').replace(' ', ''):
            self.object.tags.add(KnowledgeBaseTag.get_or_create_tag(form.cleaned_data.get('tag_2')))
        if form.cleaned_data.get('tag_3') and form.cleaned_data.get('tag_3').replace(' ', ''):
            self.object.tags.add(KnowledgeBaseTag.get_or_create_tag(form.cleaned_data.get('tag_3')))
        obj.send_file_update_email(self.request)
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(FileResourceCreateView, self).get_context_data(**kwargs)
        context.update({'resource_upload_view': True})
        return context


class FileResourceUpdateView(UpdateView):
    template_name = "2014/repository/templates/file_resource.html"
    model = FileResource
    form_class = FileResourceForm

    def get_success_url(self):
        return reverse('file_resource_list')

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not request.user.has_perm('can_edit', self.object):
            return HttpResponseForbidden("You cannot edit this resource")
        return super(FileResourceUpdateView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not request.user.has_perm('can_edit', self.object):
            return HttpResponseForbidden("You cannot edit this resource")
        return super(FileResourceUpdateView, self).post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kw = super(FileResourceUpdateView, self).get_form_kwargs()
        kw.update({
            'request': self.request,
            'file_resource': self.object
        })
        return kw

    def form_valid(self, form):
        resource_updated = False
        original_upload_time = self.object.date_uploaded
        if 'file' in form.changed_data:
            resource_updated = True
            frpv = FileResourcePreviousVersion.objects.create(current_version=self.object,
                                                              author=self.object.author,
                                                              date_uploaded = original_upload_time,
                                                              file=form.initial['file'])
        obj = form.save(commit=False)
        obj.author = self.request.user
        obj.save()
        if form.cleaned_data.get('tag_1') and form.cleaned_data.get('tag_1').replace(' ', ''):
            self.object.tags.add(KnowledgeBaseTag.get_or_create_tag(form.cleaned_data.get('tag_1')))
        if form.cleaned_data.get('tag_2') and form.cleaned_data.get('tag_2').replace(' ', ''):
            self.object.tags.add(KnowledgeBaseTag.get_or_create_tag(form.cleaned_data.get('tag_2')))
        if form.cleaned_data.get('tag_3') and form.cleaned_data.get('tag_3').replace(' ', ''):
            self.object.tags.add(KnowledgeBaseTag.get_or_create_tag(form.cleaned_data.get('tag_3')))
        self.object = obj
        if resource_updated:
            obj.send_file_update_email(self.request)
        return HttpResponseRedirect(self.get_success_url())


class FileResourceDetailView(ObjectPermissionsMixin, DetailView):
    template_name = "2014/repository/templates/file_resource.html"
    model = FileResource
    permissions = {
        'get': ['can_view']
    }


class FileResourceDownloadView(DetailView):
    template_name = "2014/repository/templates/file_resource.html"
    model = FileResource

    def get(self, request, pk, *args, **kwargs):
        self.object = self.get_object()
        if not request.user.has_perm('can_download_resource', self.object):
            return HttpResponseForbidden("You cannot download this resource")
        backing_file = self.object.file
        return return_file_response(backing_file)


class FileResourceDeleteView(ObjectPermissionsMixin, DeleteView):
    model = FileResource
    template_name = "2014/repository/templates/repository_confirm_delete.html"
    permissions = {
        'get': ['can_delete'],
        'delete': ['can_delete'],
        'post': ['can_delete']
    }

    def get_success_url(self):
        return reverse('file_resource_list')

    def get(self, request, pk, *args, **kwargs):
        self.object = self.get_object()
        if not request.user.has_perm('can_delete', self.object):
            return HttpResponseForbidden("You cannot delete this resource")
        return super(FileResourceDeleteView, self).get(request, *args, **kwargs)


class FileResourcePreviousVersionListView(ListView):
    template_name = "2014/repository/templates/repository_previous.html"
    model = FileResourcePreviousVersion
    paginate_by = 10

    def get_queryset(self):
        current_version = get_object_or_404(FileResource, pk=self.kwargs.get('current_version_pk'))
        self.current_version = current_version
        qs = FileResourcePreviousVersion.objects.filter(current_version=current_version).order_by('-date_uploaded')
        if qs.none():
            raise Http404
        query_string = self.request.META['QUERY_STRING']
        query_dict = {}
        if not query_string == "":
            if not query_string.__contains__("page"):
                if query_string.__contains__('&'):
                    split_amp = query_string.split("&")
                    #if the string has & it will give pieces in a list
                    #if not the string is passed as it is and only one element in the lsit
                    if not len(split_amp) == 1:
                        for i in split_amp:
                            x = i.split("=")
                            query_dict[x[0]] = x[1]
                    else:
                        query_dict[split_amp.split("=")[0]] = split_amp.split('=')[1]
                else:
                    query_dict[query_string.split('=')[0]] = query_string.split('=')[1]
                try:
                    qs = qs.filter(**query_dict)
                except FieldError:
                    raise Http404
        return qs

    def get(self, request, *args, **kwargs):
        if not request.user.has_perm('can_view_list', FileResourcePreviousVersion()):
            return HttpResponseForbidden("You cannot view the repository")
        return super(FileResourcePreviousVersionListView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(FileResourcePreviousVersionListView, self).get_context_data(**kwargs)
        context.update({
            'resource_previous_version_list_view': True,
            'current_version': self.current_version,
        })
        return context


class FileResourcePreviousVersionDetailView(ObjectPermissionsMixin, DetailView):
    template_name = "2014/repository/templates/repository_previous.html"
    model = FileResourcePreviousVersion
    permissions = {
        'get': ['can_view']
    }


class FileResourcePreviousVersionDownloadView(DetailView):
    template_name = "2014/repository/templates/repository_previous.html"
    model = FileResourcePreviousVersion

    def get(self, request, pk, *args, **kwargs):
        self.object = self.get_object()
        if not request.user.has_perm('can_download_resource', self.object):
            return HttpResponseForbidden("You cannot download this resource")
        backing_file = self.object.file
        return return_file_response(backing_file)


class FileResourcePreviousVersionDeleteView(ObjectPermissionsMixin, DeleteView):
    template_name = "2014/repository/templates/repository_confirm_delete.html"
    model = FileResourcePreviousVersion
    permissions = {
        'get': ['can_delete'],
        'delete': ['can_delete'],
        'post': ['can_delete']
    }

    def get_success_url(self):
        return reverse('file_resource_previous_version_list', kwargs={'current_version_pk': self.parent.pk})

    def get(self, request, pk, *args, **kwargs):
        self.object = self.get_object()
        if not request.user.has_perm('can_delete', self.object):
            return HttpResponseForbidden("You cannot delete this resource")
        return super(FileResourcePreviousVersionDeleteView, self).get(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        self.parent = self.get_object().current_version
        return super(FileResourcePreviousVersionDeleteView, self).delete(request, *args, **kwargs)