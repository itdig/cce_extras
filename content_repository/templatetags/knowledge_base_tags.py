from django import template
from repository.models import FileResource


register = template.Library()

@register.filter
def can_edit_resource(file_resource, user):
    if isinstance(file_resource, FileResource) and file_resource.can_edit(user):
        return True
    return False

@register.filter
def can_delete_resource(file_resource, user):
    if isinstance(file_resource, FileResource) and file_resource.can_delete(user):
        return True
    return False