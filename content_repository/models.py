from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from rulez import registry
from django.core.exceptions import ObjectDoesNotExist
from users.models import Staff, CommitteeMember, send_notification_email


class KnowledgeBaseCategory(models.Model):
    """
    Categories that Resources are placed into
    """

    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = "Knowledge Base Categories"

    def __unicode__(self):
        return "%s" % self.name


class KnowledgeBaseTag(models.Model):
    """
    Tags that Resources are placed into
    """

    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s" % (self.name)

    @classmethod
    def get_or_create_tag(cls, tag_name):
        new_tag = None
        try:
            new_tag = KnowledgeBaseTag.objects.get(name__iexact=tag_name)
        except ObjectDoesNotExist:
            new_tag = KnowledgeBaseTag.objects.create(name=tag_name)
        return new_tag


class FileResource(models.Model):
    """
    This class is to hold a reference to a file resource that
    was uploaded, and some associated attributes.
    """

    class Meta:
        verbose_name_plural = "Knowledge Base"

    file = models.FileField(upload_to="restricted/file_resources/")
    title = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True)
    author = models.ForeignKey(User)
    date_uploaded = models.DateTimeField(auto_now=True)
    category = models.ForeignKey(KnowledgeBaseCategory)
    tags = models.ManyToManyField(KnowledgeBaseTag)

    def __unicode__(self):
        return "%s" % self.title

    def get_absolute_url(self):
        return reverse('file_resource_detail', kwargs={'pk': self.pk})

    def can_edit(self, user_obj):
        if CommitteeMember.is_member(user_obj):
            return True
        elif Staff.is_member(user_obj):
            return True
        elif user_obj == self.author:
            return True
        return False

    def can_view(self, user_obj):
        if CommitteeMember.is_member(user_obj):
            return True
        elif Staff.is_member(user_obj):
            return True
        return False

    def can_delete(self, user_obj):
        if self.fileresourcepreviousversion_set.all().exists():
            return False
        if CommitteeMember.is_member(user_obj):
            return True
        elif Staff.is_member(user_obj):
            return True
        return False

    def can_download_resource(self, user_obj):
        if CommitteeMember.is_member(user_obj):
            return True
        elif Staff.is_member(user_obj):
            return True
        return False

    def can_view_list(self, user_obj):
        if CommitteeMember.is_member(user_obj):
            return True
        elif Staff.is_member(user_obj):
            return True
        return False

    def send_file_update_email(self, request):
        com_members = CommitteeMember.objects.filter(send_notification_emails=True,
                                                     active=True).values_list('user__pk', flat=True)
        address_list = User.objects.filter(pk__in=com_members).values_list('email', flat=True)
        send_notification_email(self, address_list, request, subject="NCORE File Resource Updated")

registry.register('can_edit', FileResource)
registry.register('can_view', FileResource)
registry.register('can_delete', FileResource)
registry.register('can_view_list', FileResource)
registry.register('can_download_resource', FileResource)


class FileResourcePreviousVersion(models.Model):
    """
    This class is to hold a reference to a file resource that
    was previously uploaded
    """

    class Meta:
        verbose_name_plural = "Knowledge Base Previous Versions"

    file = models.FileField(upload_to="restricted/file_resources/")
    author = models.ForeignKey(User)
    date_uploaded = models.DateTimeField(null=True)
    current_version = models.ForeignKey(FileResource)

    def __unicode__(self):
        return "%s" % self.current_version.title

    def get_absolute_url(self):
        return reverse('file_resource_previous_version_detail', kwargs={'current_version_pk': self.current_version.pk,
                                                                        'pk': self.pk})

    def can_edit(self, user_obj):
        if Staff.is_member(user_obj):
            return True
        return False

    def can_view(self, user_obj):
        if CommitteeMember.is_member(user_obj):
            return True
        elif Staff.is_member(user_obj):
            return True
        return False

    def can_delete(self, user_obj):
        if CommitteeMember.is_member(user_obj):
            return True
        elif Staff.is_member(user_obj):
            return True
        return False

    def can_download_resource(self, user_obj):
        if CommitteeMember.is_member(user_obj):
            return True
        elif Staff.is_member(user_obj):
            return True
        return False

    def can_view_list(self, user_obj):
        if CommitteeMember.is_member(user_obj):
            return True
        elif Staff.is_member(user_obj):
            return True
        return False

registry.register('can_edit', FileResourcePreviousVersion)
registry.register('can_view', FileResourcePreviousVersion)
registry.register('can_delete', FileResourcePreviousVersion)
registry.register('can_view_list', FileResourcePreviousVersion)
registry.register('can_download_resource', FileResourcePreviousVersion)