import os
from django.contrib import admin
from .models import KnowledgeBaseCategory, KnowledgeBaseTag, FileResource, FileResourcePreviousVersion


admin.site.register(KnowledgeBaseCategory)
admin.site.register(KnowledgeBaseTag)


class FileResourceAdmin(admin.ModelAdmin):
    def delete_model(self, request, obj):
        # delete file from file system here
        os.remove(obj.file.path)
        obj.delete()
admin.site.register(FileResource, FileResourceAdmin)


class FileResourcePreviousVersionAdmin(admin.ModelAdmin):
    def delete_model(self, request, obj):
        # delete file from file system here
        os.remove(obj.file.path)
        obj.delete()
admin.site.register(FileResourcePreviousVersion, FileResourcePreviousVersionAdmin)